cd ..

printf "\nUpdating Installers\n"
pushd installers
git pull
popd

printf "\n\nUpdating Client\n"
pushd client
git pull
popd

printf "\n\nUpdating Ansible\n"
pushd ansible
git pull
popd

printf "\n\nUpdating Terraform\n"
pushd terraform
git pull
popd

printf "\n\nUpdating Service\n"
pushd service
git pull
popd

printf "\n\nUpdating Dashboard\n"
pushd dashboard
git pull
popd

printf "\n\nUpdating Website\n"
pushd website
git pull
popd

#pushd keys
#git pull
#popd

#pushd docs
#git pull
#popd

#pushd bin
#popd

# Restart Gantry Service
sudo service gantry restart

printf "\n\nComplete\n\n"
