#!/usr/bin/env bash


##########################################
# Install Vagrant
##########################################

# Install VirtualBox
#wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
#sudo apt-get update

# Install ifupdown to fix vagrant up issues
#sudo apt-get install ifupdown

# Install VirtualBox
#sudo apt-get install -y virtualbox virtualbox-dkms virtualbox-guest-additions-iso
#sudo dpkg -i ./bin/virtualbox-5.2_5.2.6-120293_Ubuntu_xenial_amd64.deb

# Install Vagrant
#wget -nc https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb
#sudo dpkg -i vagrant_2.0.1_x86_64.deb

# Install Vagrant plugins
#vagrant plugin install vagrant-vbguest
#vagrant plugin install vagrant-hostmanager

# Install to expand VBox disk-size
#vagrant plugin install vagrant-disksize
#vagrant vbguest --do install
# Create a container
#vagrant init ubuntu/trusty64

# Launch a container
#vagrant up --provider virtualbox



vagrant up

#Give Vagrant time to load
sleep 10

# ---------------------
# Install ifupdown on Ubuntu Artful to solve networking issues
# https://github.com/cilium/cilium/issues/1918
vagrant ssh -- -t sudo apt-get install ifupdown -y
vagrant halt



echo "========================================="
echo ""
echo "  If you experience an error message above, don't worry it's cool --^"
echo "  Just wait 10 seconds & we'll restart the server for you"
echo "  - the MGMT"
echo ""
echo "========================================="

sleep 10
vagrant up --provision
# ---------------------

#Give Vagrant time to load
sleep 10

pushd ../ansible

# Install Apache
#ansible-playbook -vvvv playbooks/deploy-apache.yml -i hosts -l gantry

# Install basic tools
ansible-playbook -vvvv playbooks/install-common.yml -i hosts -l gantry

# Install Ansible
ansible-playbook -vvvv playbooks/install-ansible.yml -i hosts -l gantry

# Install Gantry Server Requirements
ansible-playbook -vvvv playbooks/install-gantry.yml -i hosts -l gantry

# Install Ubuntu Desktop
#ansible-playbook -vvvv playbooks/install-desktop.yml -i hosts -l gantry

# Restart to bring up Desktop Interface
#vagrant halt
#vagrant up

# Exit
echo "========================================="
echo ""
echo ""
echo "Installation Complete !!"
echo ""
echo "Here's a list of playbooks you may want to run next.. "
echo "eg - ansible-playbook -vvvv playbooks/install-desktop.yml -i hosts -l gantry"
echo ""
ls playbooks/
echo ""
echo "Happy Hacking :-j"
echo ""
echo ""
echo "========================================="

popd
