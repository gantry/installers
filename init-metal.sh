#!/usr/bin/env bash

ROOT_DIRECTORY="/opt/gantry/"
GANTRY_USER="ubuntu"
GANTRY_GROUP="ubuntu"
LXD_PASSWORD="g4ntry"

#echo -n "Update Repositories (y/n)? "
#read answer
#if [ "$answer" != "${answer#[Yy]}" ] ;then
#    echo Yes
#else
#    echo "Nothing to see here, moving on.."
#fi



##########################################
# Apt Update
##########################################
clear
sudo apt-get update



##########################################
# Set Permissions
##########################################

# Setup root-folder
mkdir -p $ROOT_DIRECTORY
pushd $ROOT_DIRECTORY

# Change ownership of root-folder to make life a little easier
sudo chown -R "$GANTRY_USER:$GANTRY_GROUP" "$ROOT_DIRECTORY"


##########################################
# Clone all repositories
##########################################
echo -n "Update Repositories (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    git clone https://gitlab.com/gantry/dashboard.git
    git clone https://gitlab.com/gantry/ansible.git
    git clone https://gitlab.com/gantry/terraform.git
    git clone https://gitlab.com/gantry/service.git
    git clone https://gitlab.com/gantry/client.git
#    git clone https://gitlab.com/gantry/keys.git
    #git clone https://gitlab.com/gantry/docs.git
else
    echo "Nothing to see here, moving on.."
fi


##########################################
# Configure Engility Ansible Cert
##########################################
# Work in root-folder
echo ""
echo ""
echo "Setting up Keys"
sudo chmod 600 ./keys/*.*

# Add keys to local keychain (makes ssh'ing into VMs easier)
#echo "Add keys to local .ssh keychain & update ~/.bashrc"
#echo "eval \`keychain --agents ssh --eval /opt/ansible/keys/keyname.pem\`" >> ~/.bashrc


##########################################
# Install AWS CLI & Download bin
##########################################

echo -n "Update Binary Files (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo apt-get -y install awscli
    cp -R ./keys/.aws ~/
    aws s3 cp s3://gantry-bin/ ./bin --recursive

else
    echo "Nothing to see here, moving on.."
fi



##########################################
# LXD initialization
##########################################
echo -n "Install LXD Configs (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then

    sudo usermod -a -G lxd $GANTRY_USER

    lxd init --preseed < ./installers/lxd/lxd-init-preseed.config

    sudo apt-get -y install criu

    # Setup Listener Password
    lxc config set core.trust_password $LXD_PASSWORD

    # Setup Listener Network
    lxc config set core.https_address [::]:8443

    # Add Remote system to this registry
    lxc remote add test1 10.2.3.4

    # Copy Wordpress from Local to Test1
    # lxc stop wordpress
    # lxc copy wordpress test1:wordpress

else

    echo "Nothing to see here, moving on.."
fi




##########################################
# Install Python
##########################################

echo -n "Install Python Dependencies (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then

    sudo apt-get install -y python python-pip python-dev python3 python3-pip python3-dev libffi-dev libssl-dev
    sudo apt-get install -y vagrant
    sudo apt-get install -y python3-lxc

    # Python Bindings for Vagrant & Ansible
    sudo pip3 install ansible python-vagrant ansible-vagrant flask paramiko tornado sockjs sockjs-tornado tornado_jinja2 python-vagrant ansible pymongo mongoengine simplejson gitlab python-ldap

    # LDAP
    sudo apt-get install -y python-ldap libsasl2-dev python-dev libldap2-dev libssl-dev

    # NGINX
    sudo pip3 install nginx-config-builder

else
    echo "Nothing to see here, moving on.."
fi



##########################################
# Install NGINX
##########################################
echo -n "Install NGINX (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo apt-get install -y nginx
    sudo cp ./bin/nginx/default /etc/nginx/sites-available/
    sudo service nginx restart
else
    echo "Nothing to see here, moving on.."
fi



##########################################
# Install MongoDB
##########################################
echo -n "Install Mongo (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo apt-get install -y mongodb
else
    echo "Nothing to see here, moving on.."
fi


##########################################
# Install Website, Dashboard, & Services
##########################################
echo -n "Install Gantry Tools(y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then

    pushd ./ansible
    ansible-playbook -vvvv playbooks/deploy-service.yml -i hosts -l localhost
    ansible-playbook -vvvv playbooks/deploy-dashboard.yml -i hosts -l localhost
    ansible-playbook -vvvv playbooks/deploy-website.yml -i hosts -l localhost

else
    echo "Nothing to see here, moving on.."
fi






##########################################
# Install Vagrant
##########################################

# Install VirtualBox
#wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
#sudo apt-get update

# Install ifupdown to fix vagrant up issues
#sudo apt-get install ifupdown

# Install VirtualBox
#sudo apt-get install -y virtualbox virtualbox-dkms virtualbox-guest-additions-iso
#sudo dpkg -i ./bin/virtualbox-5.2_5.2.6-120293_Ubuntu_xenial_amd64.deb

# Install Vagrant
#wget -nc https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb
#sudo dpkg -i vagrant_2.0.1_x86_64.deb

# Install Vagrant plugins
#vagrant plugin install vagrant-vbguest
#vagrant plugin install vagrant-hostmanager

# Install to expand VBox disk-size
#vagrant plugin install vagrant-disksize
#vagrant vbguest --do install
# Create a container
#vagrant init ubuntu/trusty64

# Launch a container
#vagrant up --provider virtualbox




##########################################
# Install Ansible from Repository
##########################################

#echo ""
#echo "Checking for Ansible"
#if ! (ansible --version | grep 2.2) ; then
#  echo ' '
#	echo "Adding Ansible Repository to Apt"
#	sudo apt-add-repository ppa:ansible/ansible
#	sudo apt-get update
#	sudo apt-get install ansible python python3 keychain -y
#	sudo pip3 install ansible
#  echo ' '
#fi



##########################################
# Install LXC
##########################################
#apt-get install -y lxc lxc-templates wget bridge-utils libvirt-bin
#apt-get install -y python-lxc
#apt-get install -y python3-lxc
#lxc-checkconfig

##########################################
# EXTERNAL BRIDGE Interface
# Install LXC Bridge to /etc/network/interfaces
# Allows LXC to talk to floating-network
# Remember to edit bridge_ports <interface> && IP info
# Not recommended for wifi-connections (requires manual linux-wifi config)
# For Wifi bindings, use libVirt instead..
##########################################
###Bridge  Name ###
#auto br0
#
#### Bridge Information
#iface br0 inet static
#bridge_ports em1
#bridge_stp off
#bridge_fd 9
#
#### Bridge IP ###
#address 192.168.42.10
#netmask 255.255.255.0
#network 192.168.42.0
#broadcast 192.168.42.255
#gateway 192.168.42.1
#dns-nameservers 8.8.8.8

##########################################
# EXTERNAL BRIDGE Interface
##########################################
## Turn off LXC Bridging (use libVirt instead - makes wifi up/down just work)
#sed -i -e 's/USE_LXC_BRIDGE="true"/USE_LXC_BRIDGE="false"/g' /etc/default/lxc-net
#
## Tell LXC to use Linux Bridge (enables access to outside network)
#sed -i -e 's/lxc.network.link = virbr0/lxc.network.link = br0/g' /etc/lxc/default.conf

#ifdown virbr0
#ifup virbr0


echo ""
echo "Install Complete"
echo ""
#echo 'Be sure to edit User-Specific Properties in "ansible/vars/user_vars.yml" '
#echo ""
#echo ""

# DEBUG ==================================================
# This is a better way to read inputs
#while true; do
#    read -p "Do you wish to install this program?" yn
#    case $yn in
#        [Yy]* ) make install; break;;
#
#        [Nn]* ) exit;;
#        * ) echo "Please answer yes or no.";;
#    esac
#done


