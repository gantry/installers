# Ruby class included by VagrantFile.
# Abstracts VM creation.

class Vm
  def init(config, vmName, boxName, vmIps, playbooks)

      # Configure server
      config.vm.define vmName do |vmBox|

            # Set Box Type
            vmBox.vm.box = boxName

            # Set HostName
            vmBox.vm.hostname = vmName

            # Configure Private (Laptop) network bridge
            for vmIp in vmIps
              vmBox.vm.network "private_network", ip: vmIp
            end

            # Add a DHCP Address by default
            vmBox.vm.network "private_network", type: "dhcp"

            # Add a Public Address by default
            # vmBox.vm.network "public_network", bridge: "lxdbr0", auto_config: true
            # vmBox.vm.network "public_network", bridge: 'wlan1', ip: "192.168.1.201"
            # vmBox.vm.network "public_network", bridge: 'wlan1', type: "dhcp"

            # Run Vbms Bootstrap script
            vmBox.vm.provision :shell, inline: "echo Launching vmName"
            vmBox.vm.provision :shell, path: "./vagrant/bootstrap.sh"

            # Install ifupdown on Ubuntu Artful
            #vmBox.vm.provision "shell", inline: "sudo apt-get install ifupdown"

            # Resize Filesystem from 2GB to max disk size
            # vmBox.vm.provision "shell", inline: "resize2fs /dev/sda1"

            # default router
            # vmBox.vm.provision "shell", run: "always", inline: "route add default gw 192.168.42.221 enp0s16"

            # TODO : Provisioning can't find private key
            # Stopped working when we moved to ubuntu 17.10
            # Run playbooks
            # for playbook in playbooks
            #   # Run Ansible from the Vagrant Host
            #   vmBox.vm.provision "ansible" do |ansible|
            #     ansible.verbose = "vvvv"
            #     ansible.playbook = playbook
            #   end
            # end

      end


  end
end
